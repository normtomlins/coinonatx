#!/bin/bash

#
#fix an issue with vi editor 
grep -q -F "set nocompatible" /root/.vimrc || echo 'set nocompatible' >> /root/.vimrc 

# Number of nodes we want to create on the server
num_arc_nodes=4

# We will create system accounts with this name, if we keep them under 7 characters they will
# show up in top, and you can identify each users process
user_name="coinx_"

# Max number of nodes to start while the script is running
# you will need to start the other nodes manually
#
# If you feel that this script has saved you some time, why not
# buy me a coffee?   
#
# And if you strike it rich ! I could use a new truck :)
#
# CoinonatX(XCTX) ADDRESS: CPRXdNwvU89xwQDbJH1xemHmX8pyF2PEpG
# BitCoin ADDRESS: 18oYyc9Z7txLZNFkpfv94SgGgxwFrQUvfw
# LiteCoin: LR7muE57yjhAcpXuG8qtgwL8zZ1E5pKdem
#
# Thanks !!
#
num_arc_to_start=1

# Generate a 10 character password for the users, they will all have the same password 
# you should never need to know this password.
user_password=$(cat /dev/urandom | tr -dc 'a-zA-Z0-9' | fold -w 10 | head -n 1)

# Check for swap file
swap_size=`free -m  | grep Swap | awk '{print $2}'`
mem=`free -m  | grep Mem | awk '{print $2}'`

echo "User Password: $user_password"
echo "You have $mem megs of memory for this MasterNode"
echo "You have $swap_size megs of swap space for this MasterNode"

# If we don't have a swap file create one

if [ $swap_size -le 0 ]; then
	echo "No Swap space.....creating swap space"
        sudo fallocate -l 2G /swapfile
        sudo chmod 600 /swapfile
        sudo mkswap /swapfile
        sudo swapon /swapfile
        free -m
        echo '/swapfile none swap sw 0 0' | sudo tee -a /etc/fstab
fi

# Install the required packages

if [ "$1" != "skip" ]; then
	apt-get update
	apt-get install -y automake libdb++-dev build-essential libtool autotools-dev autoconf pkg-config libssl-dev libboost-all-dev libminiupnpc-dev git
	apt-get install -y software-properties-common  python-software-properties g++ 
	add-apt-repository ppa:bitcoin/bitcoin -y
	apt-get update -y
	apt-get install libdb4.8-dev libdb4.8++-dev -y	
fi

# Find out our IP Address
ip_addr=$(ip addr show eth0 | grep "inet\b" | awk '{print $2}' | cut -d/ -f1)

# Defaults rpcport 7208 and default port 7209
rpcport=4400
port=10000


FILE="coinonatxd"

# Download what we need into the root directory
	if [ -e ~/coinonatx/CoinonatX/src/$FILE ]; then
           echo "File $FILE exists. We don't need to compile the software again"
        else
	  cd
          mkdir ~/coinonatx
          cd  ~/coinonatx
	  git clone https://github.com/Coinonat/CoinonatX.git 
	  cd  ~/coinonatx/CoinonatX/src/leveldb
	  chmod 755 *
	  cd  ~/coinonatx/CoinonatX/src
	  make -f makefile.unix
        fi

        # Copy the binary files into the system path

	if [ ! -e /usr/local/bin/$FILE ]; then
	        echo "Copy the coinonatxd to the bin directory"
		strip ~/coinonatx/CoinonatX/src/coinonatxd
		cp ~/coinonatx/CoinonatX/src/coinonatxd /usr/local/bin/
        fi
	
	# Check to make sure the file was copied
	 if [ ! -e /usr/local/bin/$FILE ]; then
                echo "Make Process must have failed as we don't have the Deamon file, please compile manually and run the script again" 
		exit
        fi


        # Clean up the root directory
	echo "Everything is going very well.... :)"
	
	#Let's go back to the root directory and create some users
	cd


# Do any clean up here
# Uncomment this if you need to run the script again, it will delete all old masternodes
# and also delete any wallets
#for (( arc_node_num=1; arc_node_num<=num_arc_nodes; arc_node_num++ ))
#do
#         # Try and stop any running deamon's so we can delete the directory's
#        su -c " coinonatxd stop" $user_name$arc_node_num
#         # Clean up any old users
#        userdel -r $user_name$arc_node_num
#        echo "Removing directory: /home/$user_name$arc_node_num"
#        rm -rf /home/$user_name$arc_node_num
#done

# Add the users
for (( arc_node_num=1; arc_node_num<=num_arc_nodes; arc_node_num++ ))
do
        echo "building CoinonatX node number: $arc_node_num"
        echo "Adding User: $arc_node_num"

        #Here we add new users
        useradd -m -p $user_password $user_name$arc_node_num
        usermod -aG sudo $user_name$arc_node_num

        #Switch to the User and install the Scripts

        # Let's get some configuration files in palce
        su -c "mkdir ~/.CoinonatX" $user_name$arc_node_num
        su -c "touch ~/.CoinonatX/coinonatx.conf" $user_name$arc_node_num

        # Create the Configuration file for the node
        count=$[count+1]

        # RCP Port only listens on the local ip 127.0.0.1
        rpcport=$[rpcport+1]

        # Port listens on the public interface
        port=$[port+1]

	# Generate a 10 character password for the rpc
	deamon_password=$(cat /dev/urandom | tr -dc 'a-zA-Z0-9' | fold -w 10 | head -n 1)

        config_str1="rpcuser=rpc_user"
        config_str2="rpcpassword=$deamon_password"
        config_str3="rpcallowip=127.0.0.1"
        config_str4="rpcport=$rpcport"
        config_str5="listen=1"
        config_str6="server=1"
        config_str7="#daemon=1"
        config_str8="staking=0"
        config_str10="port=$port"
        config_str11="masternodeaddr=$ip_addr:$port"
        config_str12="#masternode=1"
        config_str13="#masternodeprivkey=[Put Your Private Key Here]"
	config_str14="addnode=lindaminingpool.ddns.net"
	config_str15="addnode=193.70.109.114:44678"
	config_str16="addnode=94.130.99.60:44679"
	config_str17="addnode=198.13.32.63:44678"
	config_str18="addnode=108.61.247.108:44678"
        config_str="$config_str1\n$config_str2\n$config_str3\n$config_str4\n$config_str5\n$config_str6\n$config_str7\n$config_str8\n$config_str10\n$config_str11\n$config_str12\n$config_str13\n$config_str14\n$config_str15\n$config_str16\n$config_str17\n$config_str18\n"

        # Configuration File completed
        su -c "echo -e \"$config_str\" > ~/.CoinonatX/coinonatx.conf" $user_name$arc_node_num 

	# Create the READ.ME file
        config_str="This directory isn't empty, it has a hidden directory call .CoinonatX that has all your config files in it."
        su -c "echo -e \"$config_str\" > ~/READ.ME" $user_name$arc_node_num

        # Let's start it up
	# We are only going to start up num_arc_to_start node so that it can sync
	# I suggest that you start each one by one, so they can sync 
	#
	if [ $arc_node_num -le $num_arc_to_start ]
	then 
        	su -c "coinonatxd -daemon" $user_name$arc_node_num
        	echo "Waiting for Master Node # $arc_node_num to start" 
        	# if we spawn to fast, we will get errors
        	sleep 15
        	echo "Checking node # $arc_node_num "
        	su -c "coinonatxd getinfo" $user_name$arc_node_num
        	sleep 2
        	echo 
	fi

done
