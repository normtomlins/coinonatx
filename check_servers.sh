#!/bin/bash

num_nodes=4

for (( arc_num=1; arc_num<=num_nodes; arc_num++ ))
do
        echo
        echo "Check Masternode # $arc_num"
        su -c "coinonatxd getinfo" coinx_$arc_num
	sleep 1
        echo
done
